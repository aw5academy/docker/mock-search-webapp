#!/bin/bash

if [ ! -e words.js ]; then
  bash create_dict.sh
fi
docker build -t mock-search-webapp:latest .
docker run -it -p 8080:80 mock-search-webapp:latest
