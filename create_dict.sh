#!/bin/bash

NUM_WORDS=1000

sudo apt-get install wamerican

echo "var titleList = [" > words.js
for (( i=1; i<=$NUM_WORDS; i++ )); do
  random_title_len=$(shuf -i 1-5 -n 1)
  random_title="$(shuf -n${random_title_len} /usr/share/dict/american-english |sed "s,',,g" |xargs)"
  echo -n "  \"${random_title}\"" >> words.js
  if [ $i -lt $NUM_WORDS ]; then
    echo -en ",\n" >> words.js
  else
    echo -e "\n]" >> words.js
  fi
done

echo -e "\nvar urlList = [" >> words.js
for (( i=1; i<=$NUM_WORDS; i++ )); do
  domain="$(shuf -n1 /usr/share/dict/american-english |sed "s,',,g" |xargs)"
  tld="$(shuf -n1 /usr/share/dict/american-english |sed "s,',,g" |xargs |cut -c 1-3)"
  random_url="${domain}.${tld}"
  echo -n "  \"${random_url}\"" >> words.js
  if [ $i -lt $NUM_WORDS ]; then
    echo -en ",\n" >> words.js
  else
    echo -e "\n]" >> words.js
  fi
done

echo -e "\nvar descList = [" >> words.js
for (( i=1; i<=$NUM_WORDS; i++ )); do
  random_desc_len=$(shuf -i 20-30 -n 1)
  random_desc="$(shuf -n${random_desc_len} /usr/share/dict/american-english |sed "s,',,g" |xargs)"
  echo -n "  \"${random_desc}\"" >> words.js
  if [ $i -lt $NUM_WORDS ]; then
    echo -en ",\n" >> words.js
  else
    echo -e "\n]" >> words.js
  fi
done
