FROM amazonlinux:latest

ADD start.sh /root/start.sh
RUN yum install -y httpd
ADD index.html /var/www/html/index.html
RUN chmod 644 /var/www/html/index.html
RUN chmod +x /root/start.sh
ADD words.js /var/www/html/words.js
RUN chmod 644 /var/www/html/words.js
COPY css /var/www/html/css
RUN chmod 644 /var/www/html/css/*.css
EXPOSE 80
CMD ["/root/start.sh"]
